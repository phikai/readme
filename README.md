## README

Hi! My name is Kai and I'm currently a Sr. Product Manager at GitLab. This README is meant to tell you a bit more about myself and serve as a way to help describe the way I work.

## Job Details

CURRENT: Sr. Product Manager, Code Review @ GitLab.

I joined GitLab in May of 2019 as Product Manager of the Editor and Knowledge groups. I also led the Global Search group which was spun out of Editor.

If you work at GitLab you can see more about some accomplishments related to my [promotion case](https://docs.google.com/document/d/1aRLaxd9jl3oEwgjbtixxxGkNB6xh3fCjTk0VHkoCIqY/edit#).

## About me

 - I grew up outside of Houston, Texas and lived there until college.
 - I went to college in Florida and have a [BS in Hospitality Management](https://www.ucf.edu/degree/hospitality-management-bs/) from the University of Central Florida. GO KNIGHTS!
   - I also got a minor in Computer Science.
 - I attended Stetson University after my undergrad to get my MBA.
 - I met my wife in Florida and we've been married since 2009. We now live back in Texas and have two children.
 - I once had a job working at library and served as a reference librarian. I've also worked at multiple theme parks in Orlando. The rest of my professional background is on [LinkedIn](https://www.linkedin.com/in/kaiarmstrong/)
 - Lore has it that a friend of my dad's called me `phikai` long ago. It's been my online name forever and chances are high that if you see a `phikai` on the internet somewhere it's me.

## My role as a Product Manager

### Listen and Learn.

As a product manager, I try to absorb as much information as possible from users, domain experts, my counterparts, and anyone else who has an opinion.

### Strategize and Communicate.

As a product manager, I pay attention to the market, our business and our users to develop a strategy for our product. There are often many moving pieces to this that include short term improvements and longer horizon items. I try to paint this picture for the team to help align everyone in the direction we're moving.

### Make Decisions.

As a product manager, I use the inputs of information and data to make decisions to drive our vision forward. Sometimes those decisions are made collaboratively and with consensus and sometimes they aren't.

### Advocate.

As a product manager, I advocate for our product to other groups, users and the market. However, it's also equally as important to advocate for the team who builds features and make sure they're recognized and rewarded for that work.

## Communicating and working with me
_Details about this are specific to the way we work at GitLab._
 - I respond to mentions on GitLab.
 - I subscribe and try to be aware of issue labeled with groups and categories I'm working in.
 - If you open an issue, expect me to ask your opinion on how important the issue is or what other issues are impacted by it. - I want you to think like a product manager.

_Details here are related to more general work style._
 - I try to be generous with saying `thank you` and acknowledging the contributions of others. I don't respond well to that same praise coming my direction and often deflect towards how others have contributed.
 - I form opinions quickly and then continue to refine those as additional data comes in. Often, by the time I'm discussing things more openly I've had much more time to process and research so my opinions can be more strongly held.
 - I believe that bugs will be introduced and that's ok. It's important to recognize them, understand the impact and address the fixes as needed. I try to never be upset, mad or disappointed about these.
 - I generally don't believe that decisions are right and wrong. They're based on the best data available at the time and it's important to continue to evaluate and work through outcomes. Because of this, I'll struggle to admit that I was wrong or call things wrong.
 - I tend to have strong opinions about certain topics, but always try to be thoughtful when sharing these. If my delivery of these opinions feels like a source of conflict, please try to approach me regarding it.
 - I understand that we won't always know what we're getting in to and how long that might take. If research is required, I prefer time-boxed research and discovery efforts with clear exit criteria that are focused on smaller efforts where we can continue to learn.

## How you can help me

 - Remind me to smile and respond positively first.
 - Call me out when I'm not listening or not understanding. Sometimes a separate meeting can be helpful for understanding the situation.
 - Tell me to stop talking - I can dominate a conversation and sometimes I'm pretty wordy.
 - Provide a summary when you've got a long comment or discussion point. This helps to frame the important parts and makes it easier for me to digest.
 - Provide me [feedback](#feedback).


## Practical work things

 - I try to keep a normal schedule and tend to stop working around 5pm local time. I'm usually available online by 8am with advance notice, but regularly by 8:30.
 - With advance notice I'm happy to shift my schedule earlier or later to accommodate your availability.
 - I'm quick to respond via mentions on Slack so that's the best way to get immediate attention (at most hours).
 - If you have my phone number and really need me... text messages are cool.

## Personality Tests

[Strengths Finder](https://www.gallup.com/cliftonstrengths/en/strengthsfinder.aspx) - Achiever, Self-Assurance, Maximizer, Relator, Command

## Things I enjoy

I have some hobbies that I spend lots of time on and some that I don't spend much time on, but still enjoy.

### Sports:

I'm an avid fan of College Football and specifically the [UCF Knights](https://en.wikipedia.org/wiki/UCF_Knights_football). I was at the Peach Bowl in 2018 when we won a [National Title](https://en.wikipedia.org/wiki/UCF_Knights_football#National_championships).

I also enjoy keeping tabs on Nascar and Golf when seasons are regularly running. And I'll generally tune in to Playoff Baseball and the Houston Texans if they're on.

### Hobbies: 
 - Homebrewing Beer
 - Playing Video Games (streaming on [Twitch](https://twitch.tv/phikai))
 - Camping (more like Glamping)
 - Some Development, but lots of Docker Projects (check them out [Think One Zero](https://gitlab.com/thinkonezero))

### Video Games:

Most of the games I play are FPS, but I do enjoy the occasional adventure/open world game as well. This list probably changes frequently, but some games I'm currently playing are:

 - Call of Duty (Warzone specifically)
 - Valorant
 - Destiny 2 (as of Nov. 2020)
 - Pokemon TCG Online

Previously I enjoyed:

 - Legend of Zelda: Breath of the Wild
 - Fortnite
 - Star Wars Jedi: Fallen Order

### Movies:

 - Fast and Furious... ALL OF THEM.
 - Movies I've seen a lot that are on TV.

## Feedback

Want to tell me how much you love me, hate me, thing I should stop doing or start doing? You can fill out my [feedback form](https://docs.google.com/forms/d/e/1FAIpQLSd8bZL1GC8IGiZPjhuHBFXfrOabNNRUWi6RORDiOtLtXQpoNw/viewform?usp=sf_link) (anonymous) and leave some comments.

You can also fork this project and open a merge request to help clarify things in this README.
